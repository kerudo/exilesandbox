from django.db import models

class Tag(models.Model):
    value = models.CharField(max_length=400)

    def __str__(self):
        return self.value

class Mod(models.Model):
    mod_id = models.IntegerField()
    name = models.CharField(max_length=400)
    description = models.CharField(max_length=400)
    mod_type = models.CharField(max_length=400)
    ilvl = models.IntegerField()
    min1 = models.IntegerField()
    max1 = models.IntegerField()
    min2 = models.IntegerField()
    max2 = models.IntegerField()
    tags = models.ManyToManyField(Tag, related_name='associated_mods')

    def __str__(self):
        return self.name
