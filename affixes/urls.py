from django.conf.urls import url
from django.views import generic

from affixes import views

urlpatterns = [
    url(r'^mods/(?P<pk>[a-z_]+)/$', views.FilteredModList.as_view(), name='filtered-mod-list'),
    url(r'^mods/$', views.ModList.as_view(), name='mod-list'),
    url(r'^tags/(?P<pk>[0-9]+)/$', views.FilteredTagList.as_view(), name='filtered-tag-list'),
    url(r'^tags/$', views.TagList.as_view(), name='tag-list')
]
