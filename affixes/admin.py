from django.contrib import admin
from affixes.models import Mod, Tag

admin.site.register(Mod)
admin.site.register(Tag)
