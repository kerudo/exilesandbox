from django.apps import AppConfig


class AffixesConfig(AppConfig):
    name = 'affixes'
