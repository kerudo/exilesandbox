from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import status, generics

from affixes.models import Mod, Tag
from affixes.serializers import ModSerializer, TagSerializer

class ModList(generics.ListCreateAPIView):
    serializer_class = ModSerializer
    queryset = ''

    def get(self, request, format=None):
        mods = Mod.objects.all()
        serializer = ModSerializer(mods, many=True)
        return Response(serializer.data)

class FilteredModList(generics.ListCreateAPIView):
    serializer_class = ModSerializer
    queryset = ''

    def get(self, request, pk, format=None):
        mods = Mod.objects.filter(tags__value=pk)
        serializer = ModSerializer(mods, many=True)
        return Response(serializer.data)

class TagList(generics.ListCreateAPIView):
    serializer_class = TagSerializer
    queryset = ''

    def get(self, request, format=None):
        tags = Tag.objects.all()
        serializer = TagSerializer(tags, many=True)
        return Response(serializer.data)

class FilteredTagList(generics.ListCreateAPIView):
    serializer_class = TagSerializer
    queryset = ''

    def get(self, request, pk, format=None):
        tags = Tag.objects.filter(associated_mods__id=pk)
        serializer = TagSerializer(tags, many=True)
        return Response(serializer.data)
