from rest_framework import serializers
from affixes.models import Mod, Tag

class ModSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mod
        fields = ('id', 'name', 'description', 'ilvl', 'min', 'max', 'mod_id')

class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id', 'value')
