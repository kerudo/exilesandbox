import React from "react"
import { render } from "react-dom"

import {
  createStore,
  applyMiddleware,
  compose,
  combineReducers
} from "redux"
import { Provider } from "react-redux"
import thunk from "redux-thunk"

import createHistory from "history/createBrowserHistory"
import { Route } from "react-router-dom"

import {
  ConnectedRouter,
  routerMiddleware,
  push
} from "react-router-redux"

import Affixes from "./app/affixes"
import reducer from "./rootReducer"

const history = createHistory()

const middleware = [thunk, routerMiddleware(history)]

const initialState = {}

const store = createStore(
  reducer,
  initialState,
  compose(
    applyMiddleware(...middleware),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
)

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Route path="/" component={Affixes} />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('reactApp')
)
