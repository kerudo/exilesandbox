import React from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { Link } from "react-router-dom"

import "./style.scss"

class HeaderBar extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    let menu = []
    let { structure } = this.props

    for (let x in structure) {
      let submenu = []
      for (let y in structure[x]["submenu"]) {
        let path = "/mods/" + x + "/" + y
        submenu.push(
          <Link key={x + "-" + y} className="submenu-item" to={path}>
            {structure[x]["submenu"][y]["text"]}
          </Link>
        )
      }
      menu.push(
        <div key={x} className="menu-item">
          {structure[x]["text"]}
          <div className="submenu">{submenu}</div>
        </div>
      )
    }

    return(
      <div className="page-header">
        <h1>Affixes</h1>
        <div className="menu">{ menu }</div>
      </div>
    )
  }
}

HeaderBar.defaultProps = {

}

HeaderBar.propTypes = {

}

const mapStateToProps = (state, ownProps) => ({

})

export default connect(mapStateToProps)(HeaderBar)
