import React from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { Route } from "react-router-dom"

import "./style.scss"

class LandingPage extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return(
      <div className="landing-page">
        <h1>Welcome!</h1>
      </div>
    )
  }
}

LandingPage.defaultProps = {

}

LandingPage.propTypes = {

}

const mapStateToProps = (state, ownProps) => ({

})

export default connect(mapStateToProps)(LandingPage)
