import React from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { Route } from "react-router-dom"

import "./style.scss"
import {menuStructure} from "../../menuStructure"

class ModList extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    console.log(this.props.match.params)
    const subhead = [
      menuStructure[this.props.match.params.item_class]["text"],
      menuStructure[this.props.match.params.item_class]["submenu"][this.props.match.params.item_type]["text"],
    ].join(" - ")
    return(
      <div className="mod-list">
        <h1>Mod List</h1>
        <h2>{subhead}</h2>
      </div>
    )
  }
}

ModList.defaultProps = {

}

ModList.propTypes = {

}

const mapStateToProps = (state, ownProps) => ({

})

export default connect(mapStateToProps)(ModList)
