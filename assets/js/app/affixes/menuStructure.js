export const menuStructure = {
  "1h": {
    "text": "One Hand",
    "submenu": {
      "axe": {
        "text": "Axe"
      },
      "claw": {
        "text": "Claw"
      },
      "dagger": {
        "text": "Dagger"
      },
      "mace": {
        "text": "Mace"
      },
      "sceptre": {
        "text": "Sceptre"
      },
      "sword": {
        "text": "Sword"
      },
      "wand": {
        "text": "Wand"
      },
    },
  },
  "2h": {
    "text": "Two Hand",
    "submenu": {
      "axe": {
        "text": "Axe"
      },
      "bow": {
        "text": "Bow"
      },
      "mace": {
        "text": "Mace"
      },
      "staff": {
        "text": "Staff"
      },
      "sword": {
        "text": "Sword"
      },
      "fishing_rod": {
        "text": "Fishing Rod"
      },
    },
  },
  "body_armour": {
    "text": "Body Armour",
    "submenu": {
      "armour": {
        "text": "Armour"
      },
      "evasion": {
        "text": "Evasion"
      },
      "energy_shield": {
        "text": "Energy Shield"
      },
      "armour_energy_shield": {
        "text": "Armour and Energy Shield"
      },
      "armour_evasion": {
        "text": "Armour and Evasion"
      },
      "evasion_energy_shield": {
        "text": "Evasion and Energy Shield"
      },
      "sacrificial_garb": {
        "text": "Sacrificial Garb"
      },
    },
  },
  "helmet": {
    "text": "Helmet",
    "submenu": {
      "armour": {
        "text": "Armour"
      },
      "evasion": {
        "text": "Evasion"
      },
      "energy_shield": {
        "text": "Energy Shield"
      },
      "armour_energy_shield": {
        "text": "Armour and Energy Shield"
      },
      "armour_evasion": {
        "text": "Armour and Evasion"
      },
      "evasion_energy_shield": {
        "text": "Evasion and Energy Shield"
      },
      "enchantment": {
        "text": "Enchantment"
      },
    },
  },
  "gloves": {
    "text": "Gloves",
    "submenu": {
      "armour": {
        "text": "Armour"
      },
      "evasion": {
        "text": "Evasion"
      },
      "energy_shield": {
        "text": "Energy Shield"
      },
      "armour_energy_shield": {
        "text": "Armour and Energy Shield"
      },
      "armour_evasion": {
        "text": "Armour and Evasion"
      },
      "evasion_energy_shield": {
        "text": "Evasion and Energy Shield"
      },
      "enchantment": {
        "text": "Enchantment"
      },
    },
  },
  "boots": {
    "text": "Boots",
    "submenu": {
      "armour": {
        "text": "Armour"
      },
      "evasion": {
        "text": "Evasion"
      },
      "energy_shield": {
        "text": "Energy Shield"
      },
      "armour_energy_shield": {
        "text": "Armour and Energy Shield"
      },
      "armour_evasion": {
        "text": "Armour and Evasion"
      },
      "evasion_energy_shield": {
        "text": "Evasion and Energy Shield"
      },
      "enchantment": {
        "text": "Enchantment"
      },
    },
  },
  "shield": {
    "text": "Shield",
    "submenu": {
      "armour": {
        "text": "Armour"
      },
      "evasion": {
        "text": "Evasion"
      },
      "energy_shield": {
        "text": "Energy Shield"
      },
      "armour_energy_shield": {
        "text": "Armour and Energy Shield"
      },
      "armour_evasion": {
        "text": "Armour and Evasion"
      },
      "evasion_energy_shield": {
        "text": "Evasion and Energy Shield"
      },
    },
  },
  "accessories": {
    "text": "Accessories",
    "submenu": {
      "amulet": {
        "text": "Amulet"
      },
      "belt": {
        "text": "Belt"
      },
      "ring": {
        "text": "Ring"
      },
      "quiver": {
        "text": "Quiver"
      },
      "flask": {
        "text": "Flask"
      },
    },
  },
  "jewels": {
    "text": "Jewels",
    "submenu": {
      "cobalt": {
        "text": "Cobalt"
      },
      "crimson": {
        "text": "Crimson"
      },
      "viridian": {
        "text": "Viridian"
      },
      "murderous_eye": {
        "text": "Murderous Eye"
      },
      "searching_eye": {
        "text": "Searching Eye"
      },
      "hypnotcic_eye": {
        "text": "Hypnotic Eye"
      },
      "ghastly_eye": {
        "text": "Ghastly Eye"
      },
    },
  }
}
