import React from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { Route } from "react-router-dom"

import "./style.scss"
import HeaderBar from "./components/HeaderBar"
import LandingPage from "./components/LandingPage"
import ModList from "./components/ModList"

import {menuStructure} from "./menuStructure"


class Affixes extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return(
      <div className="affixes-root">
        <HeaderBar structure={menuStructure}/>
        <Route exact path="/" component={LandingPage} />
        <Route path="/mods/:item_class/:item_type" component={ModList} />
      </div>
    )
  }
}

Affixes.defaultProps = {

}

Affixes.propTypes = {

}

const mapStateToProps = (state, ownProps) => ({

})

export default connect(mapStateToProps)(Affixes)
