import {
  FETCH_MODS
} from "./types"

export const fetchMods = (key) => dispatch => {
  fetch("https://localhost:8000/api/affixes/mods/" + key + "/", {
    method: "GET",
  })
    .then(res => res.json())
    .then(data => dispatch({
      type: FETCH_MODS,
      payload: data
    }))
}
