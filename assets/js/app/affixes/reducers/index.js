import {
  FETCH_MODS
} from "../actions/types"

const initialState = {}

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_MODS:
      return {
        ...state,
        mods: action.payload
      }
    default:
      return state
  }
}
