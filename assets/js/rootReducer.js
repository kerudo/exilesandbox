import { combineReducers } from "redux"
import { routerReducer } from "react-router-redux"

import affixes from "./app/affixes/reducers"

export default combineReducers({
  affixes: affixes,
  router: routerReducer
})
